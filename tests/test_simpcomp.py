import eqsire.simpcomp as sc
import pytest
from hypothesis import given, assume
import hypothesis.strategies as st
import itertools

MAX_SIMPLEX_SIZE = 10
MAX_NUMBER_VERTICES = 10

# HELPER FUNCTIONS

@given(st.lists(st.integers()))
def test_order_sorts_integers(xs):
    assert sc.order(xs) == sorted(xs)

@given(st.lists(st.integers(), max_size=5))
def test_subfaces_contains_only_subfaces(simplex):
    subfaces = sc.subfaces(simplex)

    for subface in subfaces:
        for vertex in subface:
            assert vertex in simplex

def test_subfaces_of_empty():
    assert not list(sc.subfaces([]))

def test_subfaces_of_singleton():
    assert list(sc.subfaces([0])) == [[0]]

def test_subfaces_of_two_elements():
    subfaces = list(sc.subfaces([0,1]))
    assert [0] in subfaces
    assert [1] in subfaces
    assert [0,1] in subfaces or [1,0] in subfaces

@given(st.sets(st.integers(), max_size=5), st.integers(min_value=0, max_value=4))
def test_subfaces_of_dim_contains_only_subfaces_of_dim(simplex, dim):
    simplex = list(simplex)
    subfaces = sc.subfaces_of_dim(simplex, dim)

    for subface in subfaces:
        assert len(subface) == dim + 1
        for vertex in subface:
            assert vertex in simplex

@given(st.integers(), st.integers())
def test_subfaces_of_dim_with_two_elements(a,b):
    assume(a != b)
    subfaces = list(sc.subfaces_of_dim([a,b], 0))
    assert len(subfaces) == 2
    assert [a] in subfaces
    assert [b] in subfaces
    
# NODE

@given(st.integers(), st.lists(st.integers()), st.lists(st.integers()))
def test_adding_child(label, to_add, to_check):
    node = sc.SimplexTree.Node(label, None, {})
    for y in to_add:
        try:
            node.add_child(y)
        except ValueError:
            pass
    for z in to_check:
        if z in to_add and z >= label:
            assert node.is_child(z)
        else:
            assert not node.is_child(z)

@given(st.integers())
def test_adding_same_label(x):
    node = sc.SimplexTree.Node(x, None, {})
    node.add_child(x)
    assert not(node.is_child(x))

@given(st.integers(), st.integers())
def test_check_value(x, y):
    node = sc.SimplexTree.Node(x, None, {})
    if x > y:
        assert not node.check_value(y)
    else:
        assert node.check_value(y)

@given(st.integers())
def test_check_value_none(x):
    node = sc.SimplexTree.Node(None, None, {})
    assert node.check_value(x)

@given(st.lists(st.integers()))
def test_node_simplex(vertices):
    node = sc.SimplexTree.Node(None, None, {})
    for vertex in vertices:
        node.add_child(vertex)
        if node.label is not None and node.label < vertex:
            node = node.children[vertex]
    simplex = node.simplex()
    i = 0
    num_vertices = len(vertices)
    for vertex in simplex:
        assert i < num_vertices and vertex == vertices[i]
        while vertex == simplex[i]:
            i += 1

# ADDING FACES

@given(st.lists(st.integers(), min_size = 1, max_size=MAX_SIMPLEX_SIZE))
def test_add_simplex_adds_all_faces(vertices):

    def all_sublists(xs):
        if len(xs) == 1:
            yield xs
        else:
            for ys in all_sublists(xs[1:]):
                yield ys
                yield [xs[0]] + ys

    simpcomp = sc.SimplexTree([])
    simpcomp.add_simplex(vertices)

    for xs in all_sublists(vertices):
        assert simpcomp.is_simplex(xs)

@given(st.lists(st.lists(st.integers(min_value=1, max_value=MAX_NUMBER_VERTICES), min_size=1, max_size=MAX_SIMPLEX_SIZE)))
def test_constructor_adds_all_simplices(simplices):

    simpcomp = sc.SimplexTree(simplices)

    for simplex in simplices:
        assert simpcomp.is_simplex(simplex)

@given(st.lists(st.integers(min_value=1, max_value=MAX_NUMBER_VERTICES), min_size=1, max_size=MAX_SIMPLEX_SIZE))
def test_is_simplex_ignores_order(vertices):

    simpcomp = sc.SimplexTree([])
    simpcomp.add_simplex(vertices)

    for permutation in itertools.islice(itertools.permutations(vertices), 10):
        assert simpcomp.is_simplex(permutation)

def test_add_empty_simplex():
    simpcomp = sc.SimplexTree([])
    with pytest.raises(ValueError):
        simpcomp.add_simplex([])

@given(st.lists(st.lists(st.integers(), min_size=1, max_size=MAX_SIMPLEX_SIZE)))
def test_get_node(simplices):
    simpcomp = sc.SimplexTree(simplices)

    for simplex in simplices:
        node = simpcomp.get_node(simplex)
        node_simplex = set(node.simplex())
        assert node_simplex == set(simplex) 

@given(st.lists(st.sets(st.integers(), min_size=1, max_size=MAX_SIMPLEX_SIZE)))
def test_degree(simplices):
    simpcomp = sc.SimplexTree(simplices)

    for simplex in simplices:
        node = simpcomp.get_node(simplex)
        assert node.degree == len(simplex) - 1 

@given(st.lists(st.lists(st.integers(), min_size=1, max_size=MAX_SIMPLEX_SIZE).map(sorted)))
def test_every_vertex_in_vertex_lists(simplices):
    simpcomp = sc.SimplexTree(simplices)

    for simplex in simplices:
        for vertex in simplex:
            assert vertex in simpcomp.vertex_lists 

@given(st.lists(st.sets(st.integers(), min_size=1, max_size=MAX_SIMPLEX_SIZE)))
def test_every_vertex_degree_in_vertex_list(simplices):
    simpcomp = sc.SimplexTree(simplices)

    for simplex in simplices:
        simplex = sorted(simplex)
        for i in range(len(simplex)):
            vertex = simplex[i]
            levels = simpcomp.vertex_lists[vertex]
            assert i in levels 

@given(st.lists(st.sets(st.integers(), min_size=1, max_size=MAX_SIMPLEX_SIZE)))
def test_every_vertex_list_is_circular(simplices):
    simpcomp = sc.SimplexTree(simplices)

    max_num_simplices = sum(map(lambda x : 2**x, map(len, simplices)))
    for _, degrees in simpcomp.vertex_lists.items():
        for _, node in degrees.items():
            node0 = node
            checked = 0
            while node.sibling is not node0 and checked <= max_num_simplices:
                node = node.sibling
                checked += 1
            assert node.sibling is node0

def test_node_that_appears_twice_has_sibling():
    simpcomp = sc.SimplexTree([ [1,3], [2,3] ])
    node1 = simpcomp.root.children[1].children[3]
    node2 = simpcomp.root.children[2].children[3]
    assert node1.sibling is node2 
    assert node2.sibling is node1
