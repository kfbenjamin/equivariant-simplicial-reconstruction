"""Represent simplicial complexes with Simplex Trees"""

from typing import Any, Generator, Iterable, List
from eqsire import Simplex, SimplexGenerator


def order(simplex: Simplex) -> Simplex:
    """Sort a list representation of a simplex"""
    try:
        return sorted(simplex)
    except TypeError:
        raise TypeError("vertices must be orderable")


def subfaces(simplex: Simplex) -> SimplexGenerator:
    """Generate all (non-empty) subfaces of a simplex"""
    if simplex:
        for subface in subfaces(simplex[1:]):
            yield subface
            yield [simplex[0]] + subface
        yield [simplex[0]]


def sublists_of_size(xs: List[Any], n: int) -> List[List[Any]]:
    """Generates all sublists of xs, of length n"""
    if xs and 0 <= n <= len(xs):
        tail = xs[1:]
        for sublist in sublists_of_size(tail, n-1):
            yield [xs[0]] + sublist
        yield from sublists_of_size(tail, n)
    elif not xs and n == 0:
        yield []


def subfaces_of_dim(simplex: Simplex, dim: int) -> SimplexGenerator:
    """Generate all subfaces of a simplex of a given dimension"""
    for subface in sublists_of_size(simplex, dim + 1):
        if subface:
            yield subface


class SimplexTree:
    """Simplex Tree implementation of simplicial complex"""

    class Node:
        """Node of simplex tree, labelled with the simplex it represents"""

        def __init__(self, label, parent: 'Node', vertex_lists):

            self.label = label
            self.children = {}
            self.parent = parent
            self.vertex_lists = vertex_lists

            # Using -1 for the root eliminates some special cases
            self.degree = degree = parent.degree + 1 if parent else -1

            # Insert in circular list of nodes at this depth with this label
            if label is not None:
                if label in vertex_lists:
                    if degree in vertex_lists[label]:
                        # there is another node with this label at this depth
                        # so we insert this node in that list
                        first = vertex_lists[label][degree]
                        self.sibling = first.sibling
                        first.sibling = self
                    else:
                        # this vertex appears but not at this depth
                        vertex_lists[label][degree] = self
                        self.sibling = self
                else:
                    # this is the first such node, so we make a new list
                    vertex_lists[label] = {degree: self}
                    self.sibling = self

        def __repr__(self):
            return "Node(" + str(list(self.simplex())) + ")"

        def is_child(self, label):
            """Check if node has a child with a given label"""
            return label in self.children

        def is_child_weak(self, label):
            """Check if node has the given label, or a child with it"""
            return self.label == label or self.is_child(label)

        def add_child(self, label):
            """Add a child with specified label if one doesn't already exist"""
            if not self.check_value(label):
                raise ValueError("vertices in simplex must be sorted")
            if label != self.label and not self.is_child(label):
                self.children[label] = type(self)(
                    label, self, self.vertex_lists
                )

        def get_child(self, label) -> 'Node':
            """Access the child node with the specified label"""
            return self.children[label]

        def get_child_weak(self, label) -> 'Node':
            """Access the child node with the specified label"""
            return self if self.label == label else self.children[label]

        def check_value(self, label):
            """Check that adding a child with this label preserves ordering"""
            return self.label is None or self.label <= label

        def simplex(self) -> Simplex:
            """List of vertices in this node's corresponding simplex"""
            output = []
            node = self
            while node.label is not None:
                output = [node.label] + output
                node = node.parent
            return output

        def leaf_nodes(self):
            """Yield all leaf nodes in this subtree"""
            if self.children:
                for child in self.children.values():
                    yield from child.leaf_nodes()
            else:
                yield self

    def __init__(self, simplices: Iterable[Simplex]):

        # Pointers to circular lists. vertex_lists(i,j) points to a node
        # at depth j with label i. We maintain the invariant that all such
        # nodes are linked circularly by the sibling pointers
        self.vertex_lists = {}

        self.root = self.Node(None, None, self.vertex_lists)
        self.dimension = 0

        for simplex in simplices:
            self.add_simplex(simplex)

    def __repr__(self):
        output = "Simplicial complex of dimension "
        output += str(self.dimension)
        for dim in range(self.dimension+1):
            output += "\n"
            output += str(dim)
            output += "-simplices: "
            output += str(list(self.faces_of_dim(dim)))
        return output

    def add_simplex(self, simplex: Simplex):
        """Add a simplex and all of its faces to the complex"""

        # If we really want to be efficient we could write a function
        # to remove duplicates and sort in one pass.
        # We don't need to remove duplicates, but it makes comparing
        # dimension much easier
        simplex = order(set(simplex))

        if len(simplex) == 0:
            raise ValueError("Cannot add empty simplex")

        def add_faces(node, word):
            for i, letter in enumerate(word):
                node.add_child(letter)
                add_faces(node.get_child_weak(letter), word[i+1:])

        add_faces(self.root, simplex)

        if len(simplex) - 1 > self.dimension:
            self.dimension = len(simplex) - 1

    def get_node(self, simplex: Simplex) -> 'Node':
        """Find the node in the tree corresponding to the given simplex"""

        if not simplex:
            raise ValueError("Simplicies must contain at least one vertex")

        simplex = order(simplex)

        node = self.root
        for vertex in simplex:
            if node.label == vertex:
                pass
            elif node.is_child(vertex):
                node = node.get_child(vertex)
            else:
                raise ValueError("Simplex is not a face")
        return node

    def is_simplex(self, simplex: Simplex) -> bool:
        """Check if simplex is in the complex"""
        try:
            self.get_node(simplex)
            return True
        except ValueError:
            return False

    def is_coface(self, simplex: Simplex, coface: Simplex) -> bool:
        """Check if coface is a coface of simplex"""

        if not self.is_simplex(simplex):
            return False

        if not isinstance(coface, self.Node):
            # We could use is_simplex, but then we'd be finding the node twice
            try:
                node = self.get_node(coface)
            except ValueError:
                return False
        else:
            node = coface

        j = len(simplex) - 1
        while j >= 0 and node is not self.root:
            label = simplex[j]
            if node.label == label:
                j -= 1
            node = node.parent()
        return j < 0

    def cofaces(self, simplex: Simplex) -> SimplexGenerator:
        """Yield all cofaces of a given simplex"""

        def cofaces_at(rep, simplex=None):
            """Helper function to yield the cofaces with prefix node rep"""
            if not simplex:
                simplex = list(rep.simplex())
            if rep.children:
                for label in rep.children:
                    next_simplex = simplex + [label]
                    yield from cofaces_at(rep.get_child(label), next_simplex)
            yield simplex

        simplex = order(simplex)

        if len(simplex) <= 0:
            raise ValueError("Cannot find cofaces of empty simplex")

        if simplex[-1] in self.vertex_lists and self.is_simplex(simplex):

            last = simplex[1]
            for _, rep in self.vertex_lists[last].items():
                rep0 = rep
                if self.is_coface(simplex, rep):
                    yield from cofaces_at(rep)
                while rep.sibling is not rep0:
                    rep = rep.sibling
                    if self.is_coface(simplex, rep):
                        yield from cofaces_at(rep)

    def faces_of_dim(self, dim: int) -> SimplexGenerator:
        """Return all faces in the complex of dimension dim"""
        for vertex in self.vertex_lists:
            dims = self.vertex_lists[vertex]
            if dim in dims:
                eldest = dims[dim]
                yield eldest.simplex()
                node = eldest.sibling
                while node is not eldest:
                    yield node.simplex()
                    node = node.sibling

    def skeleton(self, n: int) -> SimplexGenerator:
        """Yield all faces in n-skeleton"""
        for dim in range(n+1):
            yield from self.faces_of_dim(dim)

    def vertices(self) -> Generator[Any, None, None]:
        """Yield all vertices"""
        for vertex in self.root.children:
            yield vertex

    def edges(self) -> SimplexGenerator:
        """Yield all edges"""
        yield from self.skeleton(1)

    def apply_vertex_map(self, vertex_map, simplex):
        """Find the induced map on simplex of a vertex map"""
        return list(map(vertex_map, simplex))

    def is_simplicial_map(self, vertex_map, other_complex):
        """Test if the function is a simplicial map"""
        # We only need to check leaf nodes in the simplex tree
        for leaf in self.root.leaf_nodes():
            image = self.apply_vertex_map(vertex_map, leaf.simplex())
            print(image)
            if not other_complex.is_simplex(image):
                return False
        return True
