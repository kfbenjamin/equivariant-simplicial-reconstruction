"""Defines the Dihedral group class"""

import eqsire.group
from collections import namedtuple

DihedralElement = namedtuple("DihedralElement", "rotations reflection")

class DihedralGroup(eqsire.group.Group):
    """The dihedral group D_{2n} of order n=order"""

    def __init__(self, order):
        self.order = order

    def product(self, g, h):
        if g.reflection:
            return DihedralElement(
                    rotations  = (g.rotations - h.rotations) % self.order,
                    reflection = not h.reflection
                    )
        return DihedralElement(
                rotations  = (g.rotations + h.rotations) % self.order,
                reflection = h.reflection
                )

    def inverse(self, g):
        if g.reflection:
            return g
        return DihedralElement(rotations = -g.rotations, reflection = False)

    def enumerate(self):
        for i in range(self.order):
            yield DihedralElement(rotations = i, reflection = False)
        for i in range(self.order):
            yield DihedralElement(rotations = i, reflection = True)
