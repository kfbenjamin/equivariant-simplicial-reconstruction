"""Compress and reconstruct simplicial complexes under group actions"""

import eqsire.simpcomp
import eqsire.group
import eqsire.action
from eqsire import Simplex


def rep(simplex: Simplex) -> Simplex:
    """Remove duplicates and sort yielding a unique representative"""
    return sorted(set(simplex))


class SimplexDictionary():
    """A dictionary which allows lists as keys"""

    def __init__(self):
        self.dictionary = {}

    def __setitem__(self, simplex, item):
        self.dictionary[tuple(rep(simplex))] = item

    def __getitem__(self, simplex):
        return self.dictionary[tuple(rep(simplex))]

    def __contains__(self, simplex):
        return tuple(rep(simplex)) in self.dictionary

    def __str__(self):
        return str(self.dictionary)


class SimplexSet():
    """A set which allows lists as keys"""

    def __init__(self):
        self.set = set()

    def add(self, simplex):
        self.set.add(tuple(rep(simplex)))

    def __contains__(self, simplex):
        return tuple(rep(simplex)) in self.set

    def __str__(self):
        return str(self.set)


class CompressedSimplexTree(eqsire.simpcomp.SimplexTree):
    """Compressed Simplicial Complex with data required to reconstruct"""

    def __init__(self, group):
        super().__init__([])
        self.stabilisers = SimplexDictionary()
        self.transfers = SimplexDictionary()
        self.group = group

    def stabiliser(self, simplex):
        """The stabiliser of the lift corresponding to simplex"""
        return self.stabilisers[simplex]

    def set_stabiliser(self, simplex, stab):
        """Set the stabiliser stab of the lift corresponding to simplex"""
        self.stabilisers[simplex] = stab

    def transfer(self, face, subface):
        """The transfer element of the face relation face > subface"""
        return self.transfers[face][subface]

    def set_transfer(self, face, subface, transfer):
        """Set the transfer element of the face relation face > subface"""
        if face not in self.transfers:
            self.transfers[face] = SimplexDictionary()
        self.transfers[face][subface] = transfer


def compress(simpcomp, debug=False):
    """Compress a simplicial complex"""

    compressed = CompressedSimplexTree(simpcomp.group)
    visited = SimplexSet()
    projection = SimplexDictionary()
    lift = SimplexDictionary()

    # adding vertices is dealt with differently
    for vertex in simpcomp.vertices():
        face = [vertex]  # vertices() gives vertices, not 0-simplices
        if face not in visited:
            compressed.add_simplex(face)

            lift[face] = face
            compressed.set_stabiliser(face, list(simpcomp.stabiliser(face)))

            for image in simpcomp.orbit(face):
                visited.add(image)
                projection[image] = face

            # There are no faces of 0-simplices so we're done

    for d in range(1, simpcomp.dimension+1):
        for face in simpcomp.faces_of_dim(d):
            if face not in visited:

                # apply projection map
                new_simplex = list(map(lambda v: projection[[v]][0], face))

                compressed.add_simplex(new_simplex)

                lift[new_simplex] = face

                compressed.set_stabiliser(new_simplex,
                                          list(simpcomp.stabiliser(face)))

                for image in simpcomp.orbit(face):
                    visited.add(image)
                    projection[image] = new_simplex

                for subface in eqsire.simpcomp.subfaces_of_dim(face, d-1):
                    proj = projection[subface]
                    translate = simpcomp.translate(subface, lift[proj])
                    compressed.set_transfer(new_simplex, proj, translate)

    if debug:
        return compressed, lift
    else:
        return compressed


def reconstruct(compressed, debug=False):
    """Reconstruct a simplicial complex from its compressed form"""

    group = compressed.group
    reconstructed = eqsire.action.SimplexTreeWithAction([],
                                                          group,
                                                          None)

    # In the paper, simplices are labelled (y,g) where y is a face and g
    # is a group element. project implements a function of type (loosely
    # speaking) simplex -> element -> simplex, taking a pair (y,g) to its
    # corresponding simplex in reconstructed
    project = SimplexDictionary()

    # when reconstructing the action, we will need to find out which
    # pair (y,g) corresponds to the vertex {n}.
    vertex_label = {}

    vertex_number = 0  # Used to assign unique labels to each vertex added
    for vertex in compressed.vertices():
        face = [vertex]
        project[face] = dict()
        stabiliser = compressed.stabiliser(face)

        M = map(lambda g: group.minrep(g, stabiliser), group.enumerate())
        M = group.uniqsort(list(M))

        for g in M:
            reconstructed.add_simplex([vertex_number])
            project[face][g] = [vertex_number]
            vertex_label[vertex_number] = (face, g)
            vertex_number += 1

    for d in range(1, compressed.dimension+1):
        for face in compressed.faces_of_dim(d):

            project[face] = dict()
            stabiliser = compressed.stabiliser(face)

            M = map(lambda g: group.minrep(g, stabiliser), group.enumerate())
            M = group.uniqsort(list(M))

            for g in M:
                g_inv = group.inverse(g)
                vertices = []
                for subface in eqsire.simpcomp.subfaces_of_dim(face, d-1):
                    transfer = compressed.transfer(face, subface)
                    for h in project[subface]:
                        x = group.product(h, group.product(g_inv, transfer))
                        if x in compressed.stabiliser(subface):
                            vertices += project[subface][h]

                vertices = rep(vertices)

                reconstructed.add_simplex(vertices)
                project[face][g] = vertices

    # now we reconstruct the group action
    def vertex_action(h, n):
        y, g = vertex_label[n]
        # h. (y,g) = (y, minrep(S(y), h.g))  
        x = group.minrep(group.product(h,g), compressed.stabiliser(y))
        return project[y][x][0]

    reconstructed.vertex_action = vertex_action

    if debug:
        return reconstructed, vertex_label
    else:
        return reconstructed

def test_isomorphism(simpcomp, reconstructed, vertex_label, lift):

    def isomorphism(vertex):
        y, g = vertex_label[vertex]
        x = lift[y]
        return simpcomp.action(g, x)[0]

    return reconstructed.is_simplicial_map(isomorphism, simpcomp)
