from typing import Any, Generator, List

Simplex = List[Any]
SimplexGenerator = Generator[Simplex, None, None]

__version__ = '0.2.0'
