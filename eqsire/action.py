"""Simplicial complex acted on by group"""

from typing import Any, List
import eqsire.simpcomp
import eqsire.group


def same_elements(xs: List[Any], ys: List[Any]) -> bool:
    """Check if xs and ys contain the same elements

    Determine whether the elements of xs are the same as the elements of ys,
    not counting multiplicity.
    """

    xs = sorted(xs)
    ys = sorted(ys)
    i = j = 0
    while i < len(xs) and j < len(ys):
        elem = xs[i]
        if elem != ys[j]:
            return False
        while i < len(xs) and xs[i] == elem:
            i += 1
        while j < len(ys) and ys[j] == elem:
            j += 1
    return i == len(xs) and j == len(ys)


class SimplexTreeWithAction(eqsire.simpcomp.SimplexTree):
    """A simplicial complex with an associated group action"""

    def __init__(self, simplices, group, vertex_action):
        super().__init__(simplices)
        self.group = group
        self.vertex_action = vertex_action

    def action(self, element, simplex):
        """The action of element on simplex"""
        return list(set(map(
            lambda v: self.vertex_action(element, v), simplex)))

    def orbit(self, simplex):
        """The orbit of simplex"""
        for element in self.group.enumerate():
            yield self.action(element, simplex)

    def stabiliser(self, simplex):
        """The stabiliser of simplex"""
        for element in self.group.enumerate():
            if same_elements(simplex, self.action(element, simplex)):
                yield element

    def translate(self, start, target):
        """A translate element that takes start to target"""
        for element in self.group.enumerate():
            if same_elements(target, self.action(element, start)):
                return element
        raise ValueError("No such translate exists")
