"""Defines the Group abstract base class"""

from abc import ABC, abstractmethod
from collections import namedtuple


class Group(ABC):
    """Abstract class representing a group"""

    @abstractmethod
    def product(self, g, h):
        """Multiply two group elements"""

    @abstractmethod
    def inverse(self, g):
        """Returns a representative of the inverse of an element"""

    @abstractmethod
    def enumerate(self):
        """Enumeration of the elements of the group"""

    def minrep(self, element, subgroup):
        """The least element (according to the enumeration) of the coset gH
            where g=element and H=subgroup"""
        coset = []
        for g in subgroup:
            coset.append(self.product(element, g))
        for g in self.enumerate():
            if g in coset:
                return g
        raise ValueError("Couldn't find minimal representative")

    def uniqsort(self, elements):
        """Sort the elements according to the enumeration"""
        matched = 0
        to_match = len(elements)
        for g in self.enumerate():
            if g in elements:
                yield g
                matched += 1
            if matched >= to_match:
                break
