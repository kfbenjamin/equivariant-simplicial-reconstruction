"""Defines the Klein group class"""

import eqsire.group

class KleinGroup(eqsire.group.Group):
    """The Klein group"""

    def product(self, g, h):
        return ((g[0] + h[0]) % 2, (g[1] + h[1]) % 2)

    def inverse(self, g):
        return g

    def enumerate(self):
        yield from [(0, 0), (0, 1), (1, 0), (1, 1)]
