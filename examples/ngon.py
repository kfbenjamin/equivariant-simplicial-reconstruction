import eqsire.dihedral
import eqsire.action
import eqsire.compress

N = 4
dihedral = eqsire.dihedral.DihedralGroup(N)

vertices = list(range(2*N))
simplices = [ (i, (i+1) % (2*N)) for i in vertices ]

def action(g, v):
    if g.reflection:
        v = (-v) % (2*N)
    return (v + (2 * g.rotations)) % (2*N)

ngon = eqsire.action.SimplexTreeWithAction(simplices, dihedral, action)

print("ORIGINAL NGON")
print(ngon)

compressed = eqsire.compress.compress(ngon)

print("COMPRESSED NGON")
print(compressed)

reconstructed = eqsire.compress.reconstruct(compressed)

print()
print("RECONSTRUCTED NGON")
print(reconstructed)
