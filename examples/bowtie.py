import eqsire.klein
import eqsire.action
import eqsire.compress

klein = eqsire.klein.KleinGroup()

# The bowtie complex
vertices = [ (-2, 1), (2,1), (-2,0), (-1,0), (0,0), (0,1), (0,2), (-2,-1), (2,-1) ]
simplices = [ [ (-2, 1), (-2, 0), (-1, 0) ],
              [ (-2,-1), (-2, 0), (-1, 0) ],
              [ ( 2, 1), ( 1, 0), ( 2, 0) ],
              [ ( 2,-1), ( 1, 0), ( 2, 0) ],
              [ (-1, 0), ( 0, 0) ],
              [ ( 1, 0), ( 0, 0) ]
            ]

def action(g, v):
    g_1, g_2 = g
    v_1, v_2 = v

    w_1 = -v_1 if g_1 else v_1
    w_2 = -v_2 if g_2 else v_2

    return w_1, w_2

bowtie = eqsire.action.SimplexTreeWithAction(simplices, klein, action)

print("ORIGINAL BOWTIE")
print(bowtie)

compressed = eqsire.compress.compress(bowtie)

print()
print("COMPRESSED BOWTIE")
print(compressed)

reconstructed = eqsire.compress.reconstruct(compressed)

print()
print("RECONSTRUCTED BOWTIE")
print(reconstructed)
