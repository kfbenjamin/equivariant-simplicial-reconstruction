WIP Python implementation of the algorithms described in the paper of Carbone,
Nanda and Naqvi [1].

Simplicial complex representation based on the work of Boissonnat and Maria [2].

\[1\]: Carbone, L., Nanda, V., & Naqvi, Y. (2018). Equivariant Simplicial Reconstruction. *arXiv preprint arXiv:1807.09396*. 

\[2\]: Boissonnat, J. D., & Maria, C. (2012, September). The Simplex Tree: An Efficient Data Structure for General Simplicial Complexes. In *European Symposium on Algorithms* (pp. 731-742). Springer, Berlin, Heidelberg.
